from django.contrib import admin

# Register your models here.
from .models import Author, Book, Publisher

class AuthorAdmin(admin.ModelAdmin):
	fields = ['email', 'first_name', 'last_name']
	list_display = ['last_name', 'first_name', 'email']
	search_fields = ['first_name', 'last_name']

class BookAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Title', {
        	'fields': ['title'],
        	'classes': ['']
        	}),
        ('Other information', {
        	'fields': ['authors', 'publisher']
        	}),
        ('Date information', {
            'fields': ['publication_date'],
            'classes': ['collapse'],
        }),
    ]
    list_display = ['title', 'publication_date', 'was_published_recently', 'publisher']
    list_filter = ['publication_date']
    search_fields = ['title']

    filter_horizontal = ['authors']

class BookInline(admin.TabularInline):
    model = Book
    extra = 1

class PublisherAdmin(admin.ModelAdmin):
    inlines = [BookInline]
    search_fields = ['name', 'website', 'city', 'country',]
    list_display = ['name', 'website', 'city', 'country',]

admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Publisher, PublisherAdmin)