from django.conf.urls import patterns, include, url
from django.contrib import admin
from hello.views import hello


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^hello/$', hello),
    url(r'^admin/', include(admin.site.urls)),
)